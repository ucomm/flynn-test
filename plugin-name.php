<?php
/*
Plugin Name: Boilerplate Plugin
Description: Minimal WP plugin boilerplate
Author: UComm Web Team
Version: 0.0.1
Text Domain: boilerplate
*/

// rename this file according to the plugin.

function test() {
  wp_enqueue_style('test-style', plugin_dir_url(__FILE__) . 'build/main.css');
  wp_enqueue_script('test-script', plugin_dir_url(__FILE__) . 'build/index.js');
}

add_action('wp_enqueue_scripts', 'test');